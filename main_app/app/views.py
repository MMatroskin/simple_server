from datetime import datetime
import os
from aiohttp import web
import aiohttp_jinja2
from main_app.app.models import DataItem
from main_app.log_srv.LogSrv import LogSrv
from main_app.shared import Result
from main_app.app_log import get_logger
from main_app.settings import DB_TYPES, ROOT_DIR, LOG_FILE, LOG_APP


class IndexView(web.View):

    async def get(self):
        context = {}
        response = aiohttp_jinja2.render_template('index.html', self.request, context)
        response.headers['Content-Language'] = 'ru'
        client_ip = self.request.remote
        log = get_logger(os.path.join(ROOT_DIR, LOG_APP), __name__)
        log.info(f'{client_ip} connected')
        return response


class DataView(web.View):

    async def get(self):
        params = self.request.rel_url.query
        data = dict(params)
        result = Result(
            False,
            'Invalid query'
        )
        query = data.get('q', '')
        if query in DB_TYPES:
            is_sql = query == DB_TYPES[0]
            obj = DataItem()
            result = await obj.get_data(is_sql)
        resp_data = {
            'success': int(result.success),
            'msg': 'Success' if result.success else 'Error',
            'data': result.message,
        }
        log = get_logger(os.path.join(ROOT_DIR, LOG_APP), __name__)
        log.debug(f"query: get({query}), success: {resp_data['msg']}")
        response = web.json_response(resp_data)
        return response

    async def post(self):
        post = await self.request.post()
        data = dict(post)
        query = data.get('q', '')
        result = Result(
            False,
            data
        )
        log_data = {
            'time': str(datetime.now()),
            'type': 'POST',
            'query': query,
            'data': data.get('data', ''),
            'success': False,
        }
        if query in DB_TYPES:
            is_sql = query == DB_TYPES[0]
            obj = DataItem(data.get('data', ''))
            result = await obj.save(is_sql)
            log_data['time'] = str(obj.time)
            log_data['success'] = result.success
        log_srv = LogSrv(os.path.join(ROOT_DIR, LOG_FILE))
        log_srv.save(log_data)
        resp_data = {
            'success': int(result.success),
            'msg': 'Success' if result.success else 'Error',
            'data': result.message,
        }
        log = get_logger(os.path.join(ROOT_DIR, LOG_APP), __name__)
        log.debug(f"query: save({query}), success: {resp_data['msg']}")
        response = web.json_response(resp_data)
        return response


class LogView(web.View):

    async def get(self):
        resp_data = {
            'success': 1,
            'msg': 'Success',
            'data': {},
        }
        try:
            ls = LogSrv(os.path.join(ROOT_DIR, LOG_FILE))
            data = ls.get_all()
            if data is not None:
                resp_data['data'] = data
        except Exception as e:
            resp_data['success'] = 0
            resp_data['msg'] = str(e)
        log = get_logger(os.path.join(ROOT_DIR, LOG_APP), __name__)
        log.debug(f"query: get(log), success: {resp_data['msg']}")
        result = web.json_response(resp_data)
        return result
