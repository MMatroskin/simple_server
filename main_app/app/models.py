from datetime import datetime
from os.path import join
from main_app.shared import Result, Data
from main_app.settings import ROOT_DIR, LOG_APP
from main_app.app_log import get_logger
from main_app.db_srv.MySqlSrv import MySqlSrv
from main_app.db_srv.SqLiteSrv import SqLiteSrv  # test
from main_app.db_srv.RedisSrv import RedisSrv


class DataItem():

    def __init__(self, data=None):
        self.log = get_logger(join(ROOT_DIR, LOG_APP), __name__)
        self.id = None
        self.result = Result(False, 'Unknown error')
        if data is not None:
            self.data = data
            self.time = datetime.now()
        else:
            self.data = None
            self.time = None

    async def save(self, is_sql=True):
        try:
            data_item = Data(self.data, self.time)
            srv = None
            if is_sql:
                srv = MySqlSrv(data=data_item)
                # srv = SqLiteSrv(data=data_item)  # test
            else:
                srv = RedisSrv(data=data_item)
            if srv is not None:
                self.result = await srv.add_data()
            return self.result
        except Exception as e:
            self.log.error("Exception", exc_info=True)
            return Result(False, str(e))

    async def get_data(self, is_sql=True):
        try:
            srv = None
            if is_sql:
                srv = MySqlSrv()
                # srv = SqLiteSrv()  # test
            else:
                srv = RedisSrv()
            if srv is not None:
                self.result = await srv.get_all_data()
            return self.result
        except Exception as e:
            self.log.error("Exception", exc_info=True)
            return Result(False, str(e))
