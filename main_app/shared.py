from collections import namedtuple


Result = namedtuple('Result', 'success, message')
Data = namedtuple('Data', 'data, time')
loggers = {}
