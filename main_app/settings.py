from os import path

SITE_HOST = '127.0.0.1'
SITE_PORT = '9090'

# sql database settings
DB_NAME = None
DB_TABLE = None
SQL_HOST = None
SQL_PORT = None
SQL_USER = None
SQL_PW = None

# redis settings
REDIS_HOST = None
REDIS_PORT = None
REDIS_DB = None
REDIS_PW = None

ROOT_DIR = path.dirname(path.abspath(__file__))
LOG_FILE = 'log.json'
LOG_APP = 'log.txt'

data_file = path.join(ROOT_DIR, 'userdata.cfg')
with open(data_file, 'r', encoding='utf-8') as hnd:
    data = hnd.readlines()
    i = 0
    while i < len(data):
        item = data[i]
        if data[i].strip() == '[sql]' and i + 6 < len(data):
            SQL_HOST = data[i + 1].strip() if data[i + 1].strip() != '' else None
            SQL_PORT = int(data[i + 2].strip()) if data[i + 2].strip() != '' else None
            DB_NAME = data[i + 3].strip()
            DB_TABLE = data[i + 4].strip()
            SQL_USER = data[i + 5].strip()
            SQL_PW = data[i + 6].strip()
            i += 7
        elif data[i].strip() == '[redis]' and i + 1 < len(data):
            REDIS_HOST = data[i + 1].strip() if data[i + 1].strip() != '' else None
            REDIS_PORT = int(data[i + 2].strip()) if i + 2 < len(data) and data[i + 2].strip() != '' else 6379
            REDIS_DB = int(data[i + 3].strip()) if i + 3 < len(data) and data[i + 3].strip() != '' else 0
            REDIS_PW = data[i + 4].strip() if i + 4 < len(data) and data[i + 4].strip() != '' else None
            i += 5
        else:
            i += 1

DB_TYPES = ('sql', 'redis')
