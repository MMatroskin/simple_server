function clearResults(){
    document.getElementById("results-container").innerHTML = '';
    document.getElementById("results-header").innerHTML = '';
}

function showResults(data, msg){
    let resultsItem = document.getElementById("results-container");
    document.getElementById("results-header").innerHTML = msg.toUpperCase();
    let jsonData = JSON.parse(data);
    if(jsonData['success'] !== 0){
    	let data = '<table>';
    	let items = jsonData['data'];
    	for(let i = 0; i < items.length; i++){
    		let ent = '<tr>\n' +
				'<td colspan="2" class="td-medium td-left">' + items[i].id + '</td>\n' +
				'<td colspan="2" class="td-nw td-left text-wrap">' + items[i].data + '</td>\n' +
				'<td colspan="2" class="td-big td-left">' + items[i].time.split('.')[0] + '</td>\n' +
				'</tr>';
			data = data + ent;
		}
    	data = data + '</table>';
    	resultsItem.innerHTML = data;
    } else {
        resultsItem.innerHTML = jsonData['message'];
    }
    // TODO: переделать на Vue
}

function showLog(data, msg){
    let resultsItem = document.getElementById("results-container");
    document.getElementById("results-header").innerHTML = msg.toUpperCase();
    let jsonData = JSON.parse(data);
    if(jsonData['success'] !== 0){
    	let data = '<table>';
    	let obj = jsonData['data'];
    	for (let key in obj){
    		let ent = '<tr>\n' +
				'<td colspan="2" class="td-big td-left">' + obj[key].time.split('.')[0] + '</td>\n' +
				'<td colspan="2" class="td-medium td-left">' + obj[key].type + '</td>\n' +
				'<td colspan="2" class="td-medium td-left">' + obj[key].query + '</td>\n' +
				'<td colspan="2" class="td-w td-left text-wrap">' + obj[key].data + '</td>\n' +
				'<td colspan="2" class="td-medium td-left">' + obj[key].success + '</td>\n' +
				'</tr>';
			data = data + ent;
		}
    	data = data + '</table>';
    	resultsItem.innerHTML = data;
    } else {
        resultsItem.innerHTML = jsonData['message'];
    }
    // TODO: переделать на Vue
}


function setMessageText(msg, msg_ext){
    let msgText = document.getElementById("messageText");
    let msgTextExt = document.getElementById("messageTextExt");
    if(msgText !== null && msgTextExt !== null){
        msgText.innerHTML = msg;
        msgTextExt.innerHTML = msg_ext;
    }
}

function messageSwitch(show, msg='', msgExt='') {
	let message = document.getElementById("message");
	if(show === false) {
		message.style.display = 'none';
	}else{
		setMessageText(msg, msgExt);
		message.style.display = 'block';
	}
}

function successMessageFunc(msg='', msgExt=''){
	messageSwitch(true, msg, msgExt);
	setTimeout(messageSwitch, window.queryTimeOut, false)
}

function errorMessageFunc(){
    let msg = 'Server not response';
	console.log(msg);
	messageSwitch(true, msg);
	setTimeout(messageSwitch, window.queryTimeOut, false)
}

function onButtonClicked(e){
	e.target.style.opacity = '0.2';
	setTimeout(() => {e.target.style.opacity = '1'},200)
}

function onContentLoaded(){
	window.queryTimeOut = 5000;

	let buttons = document.getElementsByClassName('button');
	for(let i = 0; i < buttons.length; i++){
		buttons[i].addEventListener('click', onButtonClicked)
	}

    let actionItem = document.getElementById('save-sql-button');
	if (actionItem != undefined){
		actionItem.addEventListener('click', sendSqlData)
	}
	actionItem = document.getElementById('get-sql-button');
	if (actionItem != undefined){
		actionItem.addEventListener('click', getSqlData)
	}
	actionItem = document.getElementById('get-log-button');
	if (actionItem != undefined){
		actionItem.addEventListener('click', getLog)
	}
	actionItem = document.getElementById('save-redis-button');
	if (actionItem != undefined){
		actionItem.addEventListener('click', sendRedisData)
	}
	actionItem = document.getElementById('get-redis-button');
	if (actionItem != undefined){
		actionItem.addEventListener('click', getRedisData)
	}
}

document.addEventListener('DOMContentLoaded', function(){
	onContentLoaded();
});
