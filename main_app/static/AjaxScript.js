function getData(url, timeOut, successFunc, errorFunc=errorMessageFunc, query='', async=true, type='text'){
	let data = {
        'q': query,
    };

	$.ajax({
		type: 'GET',
		url: url,
		data: data,
		dataType: type,
		cache: false,
		timeout: timeOut,
		success: function(data, status, xhr){
			console.log(data);
			if(data !== ""){
				successFunc(data, query, xhr);
			}
		},
		error: function(){
			errorFunc();
		}
	});
}

function postData(url, timeOut, successFunc, errorFunc=errorMessageFunc, data=null, async=true, type='text'){

	$.ajax({
		type: 'POST',
		url: url,
		data: data,
		async: async,
		dataType: type,
        processData: true,
		cache: false,
		timeout: timeOut,
		success: function(data, status, xhr){
			console.log(data);
			if(data !== ""){
				successFunc(data);
			}
		},
		error: function(){
			errorFunc();
		}
	});
}