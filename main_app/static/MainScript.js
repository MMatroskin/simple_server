function sendSqlData(){
    clearResults();
	sendData('sql')
}

function sendRedisData(){
    clearResults();
	sendData('redis')
}

function sendData(query){
	let url = 'post_data';
	let data = {
		'data': document.getElementById('data-action').value,
		'q': query,
	};
	document.getElementById('data-action').value = '';
	postData(url, queryTimeOut , actionSuccessFunc, errorMessageFunc, data);
}

function actionSuccessFunc(data){
	let jsonData = JSON.parse(data);
	let msg_ext = '';
    if(jsonData.success === 0){
        msg_ext = jsonData.msg_ext;
    }
	successMessageFunc(jsonData.msg, msg_ext);
}

function getLog(){
    clearResults();
	let url = 'get_log';
	let q = 'log';
	getData(url, queryTimeOut, showLog,  errorMessageFunc,q);
}

function getSqlData(){
    clearResults();
	let url = 'get_data';
	let q = 'sql';
	getData(url, queryTimeOut, showResults,  errorMessageFunc,q);
}

function getRedisData(){
    clearResults();
	let url = 'get_data';
	let q = 'redis';
	getData(url, queryTimeOut, showResults,  errorMessageFunc,q);
}
