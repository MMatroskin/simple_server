from app.views import DataView, LogView, IndexView


routes = [
    ('GET', '/', IndexView, 'index'),
    ('GET', '/get_data', DataView, 'get_data'),
    ('GET', '/get_log', LogView, 'log'),
    ('POST', '/post_data', DataView, 'set_data'),
]
