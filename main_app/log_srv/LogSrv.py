import json
import os


class LogSrv():

    def __new__(cls,  name):
        if not hasattr(cls, 'instance'):
            cls.instance = super(LogSrv, cls).__new__(cls)
        return cls.instance

    def __init__(self, file_name):
        self.item = file_name
        if not os.path.exists(self.item):
            with open(self.item, 'tw', encoding='utf-8') as fh:
                pass

    def get_all(self):
        with open(self.item, 'r', encoding='utf-8') as fh:
            try:
                result = json.load(fh)
            except Exception:
                result = None
        return result

    def save(self, data):
        log_data = self.get_all()
        key = 0
        if log_data is not None:
            keys = [int(i) for i in log_data.keys()]
            key = max(keys) + 1
        else:
            log_data = {}
        log_data[str(key)] = data
        with open(self.item, 'tw', encoding='utf-8') as fh:
            json.dump(log_data, fh, ensure_ascii=False, indent=4)
