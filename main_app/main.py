from aiohttp import web
import aiohttp_jinja2
import jinja2
from os.path import join
from settings import *
from app_log import get_logger
from routes import routes


def main():
    app = web.Application()
    for route in routes:
        app.router.add_route(route[0], route[1], route[2], name=route[3])
    app['static_root_url'] = '/static'
    app.router.add_static('/static', 'static', name='static')
    aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader('templates'))

    log = get_logger(join(ROOT_DIR, LOG_APP))

    log.debug('debug msg')

    log.info('Start server')
    web.run_app(app, host=SITE_HOST, port=SITE_PORT)
    log.info('Stop server')


if __name__ == '__main__':
    main()
