class DbSrvBase():

    def __init__(self, data=None, order='id'):
        self.data_item = data
        self.order = order

    @staticmethod
    def get_response_data(data):
        result = []
        for row in data:
            result.append(
                {
                    'id': row[0] if len(row) > 2 else '',
                    'data': row[-2],
                    'time': str(row[-1]),
                }
            )
        return result
