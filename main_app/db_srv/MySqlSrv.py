import aiomysql
import asyncio
from .DbSrvBase import DbSrvBase
from main_app.shared import Result
from os.path import join
from main_app.settings import *
from main_app.app_log import get_logger


class MySqlSrv(DbSrvBase):

    def __new__(cls, *args, **kwargs):
        if SQL_HOST is None or SQL_PORT is None:
            return None
        cls.instance = super(MySqlSrv, cls).__new__(cls)
        return cls.instance

    def __init__(self, data=None):
        super().__init__()
        self.log = get_logger(join(ROOT_DIR, LOG_APP), __name__)
        self.data_item = data

    async def add_data(self):
        loop = asyncio.get_event_loop()

        if self.data_item is not None:
            try:
                sql = f'''INSERT INTO {DB_NAME}.{DB_TABLE} (test_data, test_time) VALUES(%s, %s)'''

                conn = await aiomysql.connect(host=SQL_HOST, port=SQL_PORT,
                                              user=SQL_USER, password=SQL_PW,
                                              db=DB_NAME, loop=loop)
                async with conn.cursor() as cursor:
                    await cursor.execute(sql, (self.data_item.data, self.data_item.time))
                    await conn.commit()
                    id = cursor.lastrowid
                conn.close()
                return Result(True, f'id={id}')
            except Exception as e:
                self.log.error("Exception", exc_info=True)
                return Result(False, str(e))
        else:
            return Result(False, 'Empty data')

    async def get_all_data(self):
        loop = asyncio.get_event_loop()

        sql = f'SELECT id, test_data, test_time FROM {DB_NAME}.{DB_TABLE} ORDER BY {self.order}'
        try:
            conn = await aiomysql.connect(host=SQL_HOST, port=SQL_PORT,
                                          user=SQL_USER, password=SQL_PW,
                                          db=DB_NAME, loop=loop)
            async with conn.cursor() as cur:
                await cur.execute(sql)
                res = await cur.fetchall()
            conn.close()
            data = self.get_response_data(res)
            return Result(True, data)
        except Exception as e:
            self.log.error("Exception", exc_info=True)
            return Result(False, str(e))
