import aiosqlite
import os
from .DbSrvBase import DbSrvBase
from main_app.shared import Result
from main_app.settings import *


class SqLiteSrv(DbSrvBase):

    def __init__(self, data=None, order='id'):
        super().__init__(data, order)
        self.result_default = Result(False, 'Database not found')

    async def add_data(self):
        if self.data_item is not None:
            db_full_name = os.path.join(ROOT_DIR, DB_NAME) + '.sqlite3'
            if self.check_db(db_full_name):
                sql = f'''INSERT INTO {DB_TABLE} (id, test_data, test_time) VALUES(null, ? ,?)'''
                conn = await aiosqlite.connect(db_full_name)
                async with conn.execute(sql, (self.data_item.data, self.data_item.time)) as cursor:
                    id = cursor.lastrowid
                await conn.commit()
                await conn.close()
                return Result(True, f'id={id}')
            else:
                return self.result_default
        else:
            return Result(False, 'Empty Data')

    async def get_all_data(self):
        db_full_name = os.path.join(ROOT_DIR, DB_NAME) + '.sqlite3'
        if self.check_db(db_full_name):
            sql = f'SELECT id, test_data, test_time FROM {DB_TABLE} ORDER BY {self.order}'
            async with aiosqlite.connect(db_full_name) as conn:
                async with conn.execute(sql) as cursor:
                    res = await cursor.fetchall()
            data = self.get_response_data(res)
            return Result(True, data)
        else:
            return self.result_default

    @staticmethod
    def check_db(sqlite_db):
        res = False
        if os.path.exists(sqlite_db) and os.path.isfile(sqlite_db):
            res = True
        return res
