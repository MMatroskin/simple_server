import aioredis
import asyncio
from os.path import join
from .DbSrvBase import DbSrvBase
from main_app.settings import ROOT_DIR, LOG_APP, REDIS_HOST, REDIS_PORT, REDIS_DB, REDIS_PW
from main_app.app_log import get_logger
from main_app.shared import Result


class RedisSrv(DbSrvBase):

    def __new__(cls, *args, **kwargs):
        if REDIS_HOST is None:
            return None
        cls.instance = super(RedisSrv, cls).__new__(cls)
        return cls.instance

    def __init__(self, data=None):
        super().__init__()
        self.log = get_logger(join(ROOT_DIR, LOG_APP), __name__)
        self.data_item = data

    async def add_data(self):
        if self.data_item is not None:
            try:
                redis = await aioredis.create_redis(
                    (REDIS_HOST, REDIS_PORT),
                    db=REDIS_DB,
                    password=REDIS_PW,
                    encoding='utf-8'
                )
                await redis.set(str(self.data_item.time), self.data_item.data)
                redis.close()
                await redis.wait_closed()
                return Result(True, 'Success')
            except Exception as e:
                self.log.error("Exception", exc_info=True)
                return Result(False, str(e))
        else:
            return Result(False, 'Empty data')

    async def get_all_data(self):
        try:
            redis = await aioredis.create_redis(
                (REDIS_HOST, REDIS_PORT),
                db=REDIS_DB,
                password=REDIS_PW,
                encoding='utf-8'
            )
            data = []
            keys = await self._get_keys(redis)
            for key in keys:
                val = await redis.get(key)
                data.append(
                    {
                        'id': '-',
                        'data': val,
                        'time': key,
                    }
                )
            redis.close()
            await redis.wait_closed()
            return Result(True, data)
        except Exception as e:
            self.log.error("Exception", exc_info=True)
            return Result(False, str(e))

    @staticmethod
    async def _get_keys(redis):
        result = []
        cur = b'0'
        while cur:
            cur, tmp_keys = await redis.scan(cur)
            result.extend(tmp_keys)
        result.sort()
        return result
